# Backup-Hanoi

This program is written in the Perl Programming Language.

## Purpose

Select a backup device according to the algorithm of hanoi.

## Example Usage

List 10 cycles using 5 devices:

```bash
$ echo -e "A\nB\nC\nD\nE" > devices.txt
$ backup-hanoi devices.txt 0 9
E
A
B
A
C
A
B
A
D
A
```
